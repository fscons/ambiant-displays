FSCONS Screens
==============

Those are html pages created to be displayed on ambiant screens at
fscons 2015 but it didn't happen because of some communication issues.

foss-gbg
--------

An 'ad' for [foss-gbg](http://foss-gbg.se).

next
----

Pulls the json schedule from frab and displays the next talks.

oss2016
-------

An 'ad' for the OSS 2016 conference.
