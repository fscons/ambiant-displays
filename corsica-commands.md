# Corsica IRC command

## Show screen names

    identify screen=*

## Add tag

    admin type=subscribe tags=default screen=foobar

## Show webpage

    http://example.com screen=foobar

## Show a message for a minute

    toast tags=default timeout=6000 text="My message"
